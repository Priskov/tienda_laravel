<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('products')->insert([
            'name' => 'pingu',
            'price' => 22,
            'stock' => 3,
            'description' => 'description product',
            'image' => base64_encode(file_get_contents('/home/itb/Descargas/tux.png')),
        ]);

    }
}
